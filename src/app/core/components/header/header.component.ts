import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { HamburgerMenuComponent } from './hamburger-menu/hamburger-menu.component';
import { RouterModule } from '@angular/router';
import { SettingService } from '../../services/setting.service';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Language } from '../../constants/language';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    HamburgerMenuComponent,
    RouterModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
  ],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  #authService = inject(AuthService);
  #settingService = inject(SettingService);

  isSignedIn = this.#authService.isSignedIn;
  language = this.#settingService.language;

  showHamburgerMenu = false;

  selectLanguage(language: Language): void {
    this.#settingService.setLanguage(language);
  }

  signOut(): void {
    this.#authService.signOut();
  }

  toggleHamburgerMenu(): void {
    this.showHamburgerMenu = !this.showHamburgerMenu;
  }
}
