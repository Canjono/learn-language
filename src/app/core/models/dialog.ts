export type Dialog = {
  id: string;
  title: string;
  original: string;
  translation: string;
  info: string;
};
