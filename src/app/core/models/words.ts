export interface Word {
  id: string;
  original: string;
  translation: string;
  story: string;
  createdAt: string;
  updatedAt: string;
}
