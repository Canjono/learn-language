import { inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService {
  #authService = inject(AuthService);
  private _router = inject(Router);

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!this.#authService.isSignedIn()) {
      this._router.navigate(['/sign-in']);
      return false;
    }

    if (route.url.some((x) => x.path === 'sign-in')) {
      this._router.navigate(['/']);
      return false;
    }

    return true;
  }
}
