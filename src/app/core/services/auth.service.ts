import { Injectable, inject, signal } from '@angular/core';
import { FirebaseService } from './firebase.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  #firebaseService = inject(FirebaseService);
  #router = inject(Router);
  #snackbar = inject(MatSnackBar);

  isSignedIn = signal(false);

  constructor() {
    this.#onAuthChanges();
  }

  #onAuthChanges(): void {
    this.#firebaseService.setAuthStateChangedCallback((user) => {
      if (user) {
        this.isSignedIn.set(true);
        this.#router.navigate(['/']);
      } else {
        this.isSignedIn.set(false);
        this.#router.navigate(['/sign-in']);
      }
    });
  }

  async signIn(email: string, password: string): Promise<void> {
    try {
      await this.#firebaseService.signIn(email, password);
    } catch (error: any) {
      this.#snackbar.open(`${error.code}`, 'OK');
    }
  }

  async signOut(): Promise<void> {
    try {
      await this.#firebaseService.signOut();
    } catch (error: any) {
      this.#snackbar.open(`${error.code}`, 'OK');
    }
  }
}
