import { Injectable, inject, signal } from '@angular/core';
import { FirebaseService } from './firebase.service';
import { Dialog } from '../models/dialog';
import { FirebaseKey } from '../models/firebase';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  #firebaseService = inject(FirebaseService);
  #snackbar = inject(MatSnackBar);

  addForm = new FormGroup({
    title: new FormControl<string>('', Validators.required),
    original: new FormControl<string>('', Validators.required),
    translation: new FormControl<string>('', Validators.required),
    info: new FormControl<string>(''),
  });

  dialogs = signal<Dialog[]>([]);

  constructor() {
    this.#getDialogs();
  }

  #getDialogs(): void {
    this.#firebaseService.read<Dialog>(FirebaseKey.Dialogs).subscribe({
      next: (dialogs) => {
        this.dialogs.set(dialogs);
      },
    });
  }

  addDialog(): void {
    if (this.addForm.invalid) {
      return;
    }

    const newDialog = <Dialog>{
      id: uuidv4(),
      title: this.addForm.controls.title.value,
      original: this.addForm.controls.original.value,
      translation: this.addForm.controls.translation.value,
      info: this.addForm.controls.info.value,
    };

    const dialogs = this.dialogs();
    dialogs.push(newDialog);

    this.#firebaseService.set(FirebaseKey.Dialogs, dialogs).subscribe({
      next: () => {
        this.dialogs.set(dialogs);
      },
      error: (e) => {
        this.#snackbar.open('An error occurred', 'OK');
      },
    });
  }
}
