import { inject, Injectable, signal } from '@angular/core';

import { initializeApp } from 'firebase/app';
import {
  getAuth,
  signInWithEmailAndPassword,
  Auth,
  signOut,
  UserCredential,
  onAuthStateChanged,
  User,
} from 'firebase/auth';
import { getDatabase, ref, set, onValue } from 'firebase/database';
import { Observable } from 'rxjs';
import { FirebaseKey } from '../models/firebase';
import { environment } from '../../../environments/environment';
import { SettingService } from './setting.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  #settingService = inject(SettingService);

  #auth!: Auth;
  #database!: any;

  constructor() {
    const firebaseConfig = environment.firebaseConfig;

    const app = initializeApp(firebaseConfig);
    this.#database = getDatabase(app);
    this.#auth = getAuth();
  }

  setAuthStateChangedCallback(callbackFn: (user: unknown) => void): void {
    onAuthStateChanged(this.#auth, callbackFn);
  }

  signIn(email: string, password: string): Promise<UserCredential> {
    return signInWithEmailAndPassword(this.#auth, email, password);
  }

  signOut(): Promise<void> {
    return signOut(this.#auth);
  }

  read<T>(key: FirebaseKey): Observable<T[]> {
    return new Observable((observer) => {
      const dbRef = ref(this.#database, this.#prefixKey(key));

      onValue(dbRef, (snapshot) => {
        const value = snapshot.val() ?? [];
        observer.next(value);
        observer.complete();
      });
    });
  }

  set(key: FirebaseKey, value: any): Observable<void> {
    return new Observable((observer) => {
      set(ref(this.#database, this.#prefixKey(key)), value)
        .then(() => {
          observer.next();
          observer.complete();
        })
        .catch((e) => {
          observer.error();
        });
    });
  }

  #prefixKey(key: string): string {
    return `${this.#settingService.language()}/${key}`;
  }
}
