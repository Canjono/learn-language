import { Injectable, signal } from '@angular/core';
import { Language } from '../constants/language';

@Injectable({
  providedIn: 'root',
})
export class SettingService {
  #defaultLanguage: Language = 'russian';

  language = signal<string | null>(null);

  constructor() {
    this.#initLanguage();
  }

  #initLanguage(): void {
    let language = localStorage.getItem('language');

    if (!language) {
      language = this.#defaultLanguage;
      localStorage.setItem('language', language);
    }

    this.language.set(language);
  }

  setLanguage(language: Language): void {
    localStorage.setItem('language', language);
    this.language.set(language);
  }
}
