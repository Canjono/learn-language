import { effect, inject, Injectable, signal } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { Word } from '../models/words';
import { FirebaseService } from './firebase.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FirebaseKey } from '../models/firebase';
import { SettingService } from './setting.service';

@Injectable({
  providedIn: 'root',
})
export class WordService {
  #formBuilder = inject(FormBuilder);
  #firebaseService = inject(FirebaseService);
  #snackbar = inject(MatSnackBar);
  #settings = inject(SettingService);

  words = signal<Word[]>([]);

  addWordForm = this.#formBuilder.group({
    original: ['', Validators.required],
    translation: ['', Validators.required],
    story: [''],
  });

  updateWordForm = this.#formBuilder.group({
    original: ['', Validators.required],
    translation: ['', Validators.required],
    story: [''],
  });

  constructor() {
    this.#getWords();

    effect(
      () => {
        this.#settings.language();
        this.#getWords();
      },
      { allowSignalWrites: true }
    );
  }

  #getWords(): void {
    this.#firebaseService.read<Word>(FirebaseKey.Words).subscribe({
      next: (words) => {
        words.sort((a, b) => {
          const dateA = new Date(a.createdAt);
          const dateB = new Date(b.createdAt);

          if (dateA > dateB) return -1;
          if (dateB > dateA) return 1;

          return 0;
        });

        this.words.set(words);
      },
    });
  }

  add(): void {
    if (this.addWordForm.invalid) {
      return;
    }

    const word = <Word>{
      id: uuidv4(),
      original: this.addWordForm.controls.original.value,
      translation: this.addWordForm.controls.translation.value,
      story: this.addWordForm.controls.story.value ?? '',
      createdAt: JSON.stringify(new Date()).replace(/"/g, ''),
      updatedAt: JSON.stringify(new Date()).replace(/"/g, ''),
    };

    const words = [...this.words(), word];

    this.#firebaseService.set(FirebaseKey.Words, words).subscribe({
      next: () => {
        this.addWordForm.reset();
        this.words.set(words);
      },
      error: (e) => {
        this.#snackbar.open('An error occured', 'OK');
      },
    });
  }

  initUpdateWord(id: string): void {
    const word = this.words().find((word) => word.id === id);

    if (!word) {
      return;
    }

    const controls = this.updateWordForm.controls;
    controls.original.setValue(word.original);
    controls.translation.setValue(word.translation);
    controls.story.setValue(word.story);
  }

  updateWord(id: string): void {
    if (this.updateWordForm.invalid) {
      return;
    }

    const word = this.words().find((word) => word.id === id);

    if (!word) {
      return;
    }

    word.original = this.updateWordForm.controls.original.value!;
    word.translation = this.updateWordForm.controls.translation.value!;
    word.story = this.updateWordForm.controls.story.value ?? '';
    word.updatedAt = JSON.stringify(new Date()).replace(/"/g, '');

    this.#firebaseService.set(FirebaseKey.Words, this.words()).subscribe({
      error: (e) => {
        this.#snackbar.open('An error occured', 'OK');
      },
    });
  }

  async delete(deletedWord: Word): Promise<void> {
    if (
      confirm(
        `Are you sure you want to delete the word: ${deletedWord.original}?`
      )
    ) {
      const remainingWords = this.words().filter(
        (word) => word.id !== deletedWord.id
      );

      this.#firebaseService.set(FirebaseKey.Words, remainingWords).subscribe({
        next: () => {
          this.words.set(remainingWords);
        },
      });
    }
  }
}
