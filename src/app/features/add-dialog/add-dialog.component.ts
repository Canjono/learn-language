import { Component, inject } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { DialogService } from '../../core/services/dialog.service';

@Component({
  selector: 'app-add-dialog',
  standalone: true,
  imports: [ReactiveFormsModule, MatButtonModule],
  templateUrl: './add-dialog.component.html',
  styleUrl: './add-dialog.component.scss',
})
export class AddDialogComponent {
  #dialogService = inject(DialogService);

  form = this.#dialogService.addForm as FormGroup;

  add(): void {
    this.#dialogService.addDialog();
  }
}
