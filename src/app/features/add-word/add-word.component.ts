import {
  ChangeDetectionStrategy,
  Component,
  inject,
  OnInit,
} from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { WordService } from '../../core/services/word.service';
import { SettingService } from '../../core/services/setting.service';

@Component({
  selector: 'app-add-word',
  standalone: true,
  imports: [ReactiveFormsModule, MatButtonModule],
  templateUrl: './add-word.component.html',
  styleUrls: ['./add-word.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddWordComponent implements OnInit {
  #wordService = inject(WordService);
  #settings = inject(SettingService);

  form = this.#wordService.addWordForm as FormGroup;
  language = this.#settings.language;

  ngOnInit(): void {}

  add(): void {
    this.#wordService.add();
  }
}
