import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { Dialog } from '../../../core/models/dialog';

@Component({
  selector: 'app-dialog-dialog',
  standalone: true,
  imports: [MatButtonModule, MatDialogModule],
  templateUrl: './dialog-dialog.component.html',
  styleUrl: './dialog-dialog.component.scss',
})
export class DialogDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: { dialog: Dialog }) {}
}
