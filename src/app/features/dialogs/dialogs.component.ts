import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService } from '../../core/services/dialog.service';
import { MatListModule } from '@angular/material/list';
import { MatDialog } from '@angular/material/dialog';
import { Dialog } from '../../core/models/dialog';
import { DialogDialogComponent } from './dialog-dialog/dialog-dialog.component';

@Component({
  selector: 'app-dialogs',
  standalone: true,
  imports: [CommonModule, MatListModule],
  templateUrl: './dialogs.component.html',
  styleUrl: './dialogs.component.scss',
  providers: [DialogService],
})
export class DialogsComponent {
  #dialogsService = inject(DialogService);
  #matDialog = inject(MatDialog);

  dialogs = this.#dialogsService.dialogs;

  openDialog(dialog: Dialog): void {
    this.#matDialog.open(DialogDialogComponent, {
      data: {
        dialog,
      },
      minWidth: 300,
    });
  }
}
