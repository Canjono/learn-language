import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { FlashcardService } from './services/flashcard.service';
import { Flashcard, FlashcardSide } from './models/flashcard';
import { SettingService } from '../../core/services/setting.service';

@Component({
  selector: 'app-flashcards',
  standalone: true,
  imports: [MatButtonModule],
  templateUrl: './flashcards.component.html',
  styleUrls: ['./flashcards.component.scss'],
  providers: [FlashcardService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FlashcardsComponent {
  #flashcardService = inject(FlashcardService);
  #settings = inject(SettingService);

  language = this.#settings.language;
  flashcardSide = FlashcardSide;

  get currentCard(): Flashcard | null {
    return this.#flashcardService.currentCard;
  }

  onStart(language: string): void {
    this.#flashcardService.start(language);
  }

  onNext(): void {
    this.#flashcardService.next();
  }

  onTurnAround(): void {
    this.#flashcardService.turnAround();
  }
}
