import { Injectable, inject } from '@angular/core';
import { Flashcard } from '../models/flashcard';
import { WordService } from '../../../core/services/word.service';

@Injectable()
export class FlashcardService {
  #wordService = inject(WordService);

  #shuffledCards: Flashcard[] = [];
  #currentIndex = 0;

  currentCard!: Flashcard | null;

  start(language: string): void {
    this.#shuffledCards = this.#wordService
      .words()
      .map((word) => new Flashcard(word, language))
      .sort(() => Math.random() - 0.5);

    this.#currentIndex = 0;
    this.currentCard = this.#shuffledCards[this.#currentIndex];
  }

  next(): void {
    if (++this.#currentIndex >= this.#shuffledCards.length) {
      this.currentCard = null;
      return;
    }

    this.currentCard = this.#shuffledCards[this.#currentIndex];
  }

  turnAround(): void {
    this.currentCard?.turnAround();
  }
}
