import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MoreOptionsComponent } from './more-options/more-options.component';
import { WordCardComponent } from './word-card/word-card.component';
import { WordService } from '../../core/services/word.service';

@Component({
  selector: 'app-list-words',
  standalone: true,
  imports: [MoreOptionsComponent, WordCardComponent],
  templateUrl: './list-words.component.html',
  styleUrls: ['./list-words.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListWordsComponent {
  #wordService = inject(WordService);

  showOriginal = false;
  showTranslations = false;
  inEditMode = false;
  showMoreOptions = false;

  words = this.#wordService.words;

  toggleOriginal(): void {
    this.showOriginal = !this.showOriginal;
  }

  toggleTranslations(): void {
    this.showTranslations = !this.showTranslations;
  }

  toggleEditMode(): void {
    this.inEditMode = !this.inEditMode;
  }

  toggleMoreOptions(): void {
    this.showMoreOptions = !this.showMoreOptions;
  }
}
