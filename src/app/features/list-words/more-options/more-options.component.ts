import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  inject,
  signal,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { SettingService } from '../../../core/services/setting.service';

@Component({
  selector: 'app-more-options',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, MatMenuModule],
  templateUrl: './more-options.component.html',
  styleUrls: ['./more-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoreOptionsComponent implements OnInit {
  #settings = inject(SettingService);

  @Input() showOriginal!: boolean;
  @Input() showTranslations!: boolean;

  @Output() toggleOriginal = new EventEmitter<void>();
  @Output() toggleTranslations = new EventEmitter<void>();
  @Output() toggleEditMode = new EventEmitter<void>();

  language = this.#settings.language;

  ngOnInit(): void {}

  onToggleOriginal(): void {
    this.toggleOriginal.emit();
  }

  onToggleTranslations(): void {
    this.toggleTranslations.emit();
  }

  onToggleEditMode(): void {
    this.toggleEditMode.emit();
  }
}
