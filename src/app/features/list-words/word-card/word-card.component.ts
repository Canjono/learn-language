import { MatDialog } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  inject,
  Input,
} from '@angular/core';
import { Word } from '../../../core/models/words';
import { WordService } from '../../../core/services/word.service';
import { WordDialogComponent } from '../word-dialog/word-dialog.component';

@Component({
  selector: 'app-word-card',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, RouterModule],
  templateUrl: './word-card.component.html',
  styleUrls: ['./word-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WordCardComponent implements OnInit {
  #wordService = inject(WordService);
  #dialog = inject(MatDialog);

  @Input() word!: Word;
  @Input() showOriginal!: boolean;
  @Input() showTranslations!: boolean;
  @Input() inEditMode!: boolean;

  ngOnInit(): void {}

  delete(): void {
    this.#wordService.delete(this.word);
  }

  openDialog(): void {
    this.#dialog.open(WordDialogComponent, {
      data: { word: this.word },
      minWidth: 300,
    });
  }
}
