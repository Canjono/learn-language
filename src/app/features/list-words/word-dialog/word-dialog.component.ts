import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { Word } from '../../../core/models/words';

@Component({
  selector: 'app-word-dialog',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule],
  templateUrl: './word-dialog.component.html',
  styleUrls: ['./word-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WordDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: { word: Word }) {}

  ngOnInit(): void {}
}
